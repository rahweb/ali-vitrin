<html lang="en">
    <?php include("blocks/head.php");?>
    <body>
        <?php include("blocks/menu.php");?>
        <div class="row login">
            <div class="container">
                <ul class="nav nav-pills nav-fill navtop">
                    <li class="nav-item">
                        <a class="nav-link active" href="#menu1" data-toggle="tab">ورود</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#menu2" data-toggle="tab">ثبت نام</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" role="tabpanel" id="menu1">
                        <div class="wrapper fadeInDown">
                            <div id="formContent">
                                <!-- Tabs Titles -->
                                <!-- Icon -->
                                <div class="fadeIn first">
                                    <h4>ورود به علی ویترین</h4>
                                    <br>
                                    <img src="images/logo_fars7i.png" id="icon" alt="User Icon" />
                                    <br>
                                    <br>
                                </div>
                                <!-- Login Form -->
                                <form>
                                    <input type="text" id="login" class="fadeIn second" name="login" placeholder="نام کاربری">
                                    <input type="text" id="password" class="fadeIn third" name="login" placeholder="رمز ورود">
                                    <div class="col-md-12 refereshrecapcha">
                                        <img src="http://alivitrin:8888/captcha/default?uKwBBMem">
                                    </div>
                                    <div class="container log">
                                        <div class="row refresh">
                                            <div class="col-md-2">
                                                <a href="">
                                                    <img src="images/refresh.png" alt="">
                                                </a>
                                            </div>
                                            <div class="col-md-10">
                                                <input style="width:97.5%" type="text" id="password" class="fadeIn third" name="login" placeholder="کد امنیتی">
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <input type="submit" class="fadeIn fourth" value="ورود">
                                </form>
                                <!-- Remind Passowrd -->
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="menu2">
                        <div class="wrapper fadeInDown">
                            <div id="formContent">
                                <!-- Tabs Titles -->
                                <!-- Icon -->
                                <div class="fadeIn first">
                                <h4>ثبت نام در علی ویترین</h4>
                                    <br>
                                    <img src="images/logo_fars7i.png" id="icon" alt="User Icon" />
                                    <br>
                                    <br>
                                </div>
                                <!-- Login Form -->
                                <form>
                                    <input type="text" id="login" class="fadeIn second" name="login" placeholder="نام">
                                    <input type="text" id="password" class="fadeIn third" name="login" placeholder="شماره همراه">
                                    <input type="text" id="password" class="fadeIn third" name="login" placeholder="ایمیل">
                                    <input type="text" id="password" class="fadeIn third" name="login" placeholder="پسورد">
                                    <input type="submit" class="fadeIn fourth" value="ثبت نام">
                                </form>
                                <!-- Remind Passowrd -->
                                <div id="formFooter">
                                    <p>لطفا جهت ثبت نام در سایت با مدیریت بازاریابی شماره 09193714211 تماس بگیرید</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include("blocks/footer.php");?>
        <?php include("blocks/script.php");?>
    </body>
</html>