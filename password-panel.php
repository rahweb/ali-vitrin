<html lang="en">
    <?php include("blocks/head.php");?>
    <body>
        <?php include("blocks/menu.php");?>
        <div class="profile">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <?php include("panel/control-panel.php");?>
                    </div>
                    <div class="col-lg-9 well-l">
                        <div class="row" style="margin-right: 2%;padding-top: 2%;">
                            <h4>تغییر رمز عبور</h4>
                        </div>
                        <hr>
                        <h6 style="margin-right: 2%;">ویرایش رمز عبور</h6>
                        <div class="row edit-form">
                            <div class="container">
                                <input id="name" name="name" type="text" placeholder="رمز عبور جدید" class="form-control margin">
                                <input id="name" name="name" type="text" placeholder="تکرار رمز عبور جدید" class="form-control margin">
                                <button type="submit" class="btn btn-default grey-1">ثبت فاکتور</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include("blocks/footer.php");?>
        <?php include("blocks/script.php");?>
    </body>
</html>