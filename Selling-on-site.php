<html lang="en">
    <?php include("blocks/head.php");?>
    <body>
        <?php include("blocks/menu.php");?>
        <div class="how-to-register">
            <div class="container">
                <br>
                <div class="row">
                    <div class="brad">
                        <ul>
                            <li class="home"><a href="">علی ویترین</a></li>
                            <li class="last"><a>فروش در علی ویترین</a></li>
                        </ul>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="title">
                        <h5>فروش وتبلیغات در علی ویترین</h5>
                        <hr class="hr">
                        <p>سایت علی ویترین یک سایت تخصصی در ضمینه ی کفش وصنایع وایسته می باشد,که دارای دسته کلی محصولات کفش , مواد اولیه و خدمات تولید میباشد.</p>
                        <p>تولیدکنندگان و تامیین کنندگان عزیز میتوانند باعلی ویترین دربخش تبلیغات و فروش از طریق سایت اقدام به همکاری نمایید.</p>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="title">
                        <h5>امکانات سایت دربخش تبلیغات وفروش</h5>
                        <hr class="hr">
                    </div>
                    <div class="row icon text-center">
                        <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12">
                            <img src="images/icon/Untitled-1.jpg" alt="">
                            <hr>
                            <p>
                                ۱-معرفی برند و محصولات دریک سایت اختصاصی کفش به چهار زبان
                                <br>
                                (فارسی,عربی,روسی,انگلیسی)
                            </p>
                        </div>
                        <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12">
                            <img src="images/icon/Untitled-2.png" alt="">
                            <hr>
                            <p>دسته بندی برای وارد کردن محصول خود کاربر در هر ساعت از شبانه روز</p>
                            <br>
                        </div>
                        <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12">
                            <img src="images/icon/Untitled-4.png" alt="">
                            <hr>
                            <p>بهینه شدن نام برند در موتورهای جست و جو اینترنتی</p>
                            <br>
                        </div>
                        <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12">
                            <img src="images/icon/Untitled-5.png" alt="">
                            <hr>
                            <p>امکان قرار دادن ۵ محصول و ۱۰ عکس برای هر محصول</p>
                            <br>
                        </div>
                        <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12">
                            <img src="images/icon/Untitled-8.png" alt="">
                            <hr>
                            <p>عکاسی تعدادی از محصولات در آتلیه علی ویترین</p>
                            <br>
                        </div>
                        <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12">
                            <img src="images/icon/Untitled-9.png" alt="">
                            <hr>
                            <p>پشتیبانی یکساله خدمات سایت</p>
                            <br>
                        </div>
                        <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12">
                            <img src="images/icon/Untitled-3.png" alt="">
                            <hr>
                            <p>ترجمه خودکار جزییات محصولات به زبانهای سایت</p>
                            <p>تستی تستی تستی تستی تستی تستی  تستی تستی تستی  تستی تستی تستی تستی تستی تستی  تستی تستی تستی  تستی تستی تستی  تستی تستی تستی  تستی تستی تستی  
                            تستی تستی تستی  تستی تستی تستی  تستی تستی تستی تستی تستی تستی  تستی تستی تستی 
                            </p>
                            <br>
                        </div>
                        <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12">
                            <img src="images/icon/Untitled-6.png" alt="">
                            <hr>
                            <p>فروش نقد و بدون واسطه در سایت</p>
                            <p>تستی تستی تستی تستی  تستی تستی تستی تستی تستی تستی  تستی تستی تستی  تستی تستی تستی  تستی تستی تستی  تستی تستی تستی 
                            تستی تستی تستی  تستی تستی تستی  تستی تستی تستی  تستی تستی تستی  تستی تستی تستی  تستی تستی تستی  تستی تستی تستی  
                            </p>
                            <br>
                        </div>
                        <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12">
                            <img src="images/icon/Untitled-7.png" alt="">
                            <hr>
                            <p>اطلاع رسانی محصول جدید قابل فروش به خریداران همان دسته کاری</p>
                            <p>درسایت علی ویترین چنانچه محصول جدیدی برای فروش بارگذاری شود 
                                برای مثال دسته مجلسی مردانه به تمام خریداران کفش دربات تلگرامی که به صورت رایگان دردسته مجلسی مردانه ثبت نام کرده اند
                                این محصول جدید بصورت اتوماتیک ارسال میگردد وآن ها میتوانند محصول جدید را درسایت خریداری نمایند.
                            </p>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row step text-center">
                    <br>
                    <br>
                    <div class="title text-left">
                        <h5>مراحل فروش و تبلیغات در علی ویترین</h5>
                        <hr class="hr">
                        <br>
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <img src="images/step/file.png" alt="">
                            <hr>
                        </div>
                        <div class="col-md">
                            <img class="ho" src="images/fgfg.png" alt="">
                        </div>
                        <div class="col-md">
                            <img src="images/step/mail.png" alt="">
                            <hr>
                        </div>
                        <div class="col-md">
                            <img class="ho" src="images/fgfg-1.png" alt="">
                        </div>
                        <div class="col-md">
                            <img src="images/step/contract.png" alt="">
                            <hr>
                        </div>
                        <div class="col-md">
                            <img class="ho" src="images/fgfg.png" alt="">
                        </div>
                        <div class="col-md">
                            <img src="images/step/portfolio.png" alt="">
                            <hr>
                        </div>
                        <div class="col-md">
                            <img class="ho" src="images/fgfg-1.png" alt="">
                        </div>
                        <div class="col-md">
                            <img src="images/step/sale.png" alt="">
                            <hr>
                        </div>
                    </div>
                    <div class="row text-tt">
                        <div class="col-md" style="margin-right: -5%;">
                            <h5 class="bottom">مرحله اول</h5>
                            <p>
                                تکمیل فرم ثبت نام با تماس تلفنی
                                <br>
                                با شماره
                                ۰۲۱۶۶۳۲۴۷۶۶
                            </p>
                        </div>
                        <div class="col-md">
                            <h5 class="top">مرحله دوم</h5>
                            <p>
                                تاییدیه ثبت نام و گرفتن پنل کاربری
                            </p>
                        </div>
                        <div class="col-md">
                            <h5 class="bottom">مرحله سوم</h5>
                            <p>
                                انعقاد قرارداد باعلی ویترین
                            </p>
                        </div>
                        <div class="col-md">
                            <h5 class="top">مرحله چهارم</h5>
                            <p>
                                آموزش وپر کردن اطلاعات فروشگاه و وارد کردن محصولات
                            </p>
                        </div>
                        <div class="col-md" style="margin-left: -5%;">
                            <h5 class="bottom">مرحله پنجم</h5>
                            <p>
                                شروع تبلیغات و فروش
                            </p>
                        </div>
                    </div>
                    <div class="button">
                        <br>
                        <button type="button" class="btn btn-info grey-1">
                            تکمیل فرم تبلیغات و فروش تامیین کنندگان
                        </button>
                    </div>
                </div>
                <br>
            </div>
        </div>
        <?php include("blocks/footer.php");?>
        <?php include("blocks/script.php");?>
    </body>
</html>