<html lang="en">
    <?php include("blocks/head.php");?>
    <body>
        <?php include("blocks/menu.php");?>

        <div class="brand-page">
            <div class="container bg-brand">
                <div class="row" style="padding:0px 15px">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="br">
                            <img src="images/cl1.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="shr">
                            <div class="row">
                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 text-center">
                                    <h2>کفش نوین چرم</h2>
                                </div>
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 text-center">
                                    <a href="">
                                        <span class="flaticon flaticon-share"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="img">
                        <?php include("blocks/slider.php");?>
                    </div>
                </div>
                <div class="tab">
                    <div class="row">
                        <ul class="nav nav-pills nav-fill navtop">
                            <li class="nav-item">
                                <a class="nav-link active" href="#menu1" data-toggle="tab">درباره کفش سیاره</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#menu2" data-toggle="tab">ارسال پیام</a>
                            </li>
                        </ul>
                        <div class="tab-content float-right">
                            <div class="tab-pane active" role="tabpanel" id="menu1">
                                <p>
                                نوشته تستی برای علی ویترن نوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترین نوشته. 
                                </p>
                                <p>
                                نوشته تستی برای علی ویترن نوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترننوشته تستی برای علی ویترین نوشته. 
                                </p>
                            </div>
                            <div class="tab-pane" role="tabpanel" id="menu2">
                                <div class="hos">
                                    <div class="row">
                                        <form role="form" id="contact-form" class="contact-form">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="Name" autocomplete="off" id="Name" placeholder="نوشتن نام">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="email" class="form-control" name="email" autocomplete="off" id="email" placeholder="نوشتن ایمیل">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <textarea class="form-control textarea" rows="3" name="Message" id="Message" placeholder="نوشتن پیام"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn main-btn pull-right">ارسال پیام</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="information" style="width:100%;">
                        <div class="title">
                            <h5>اطلاعات تماس</h5>
                        </div>
                        <br>
                        <div class="infor">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="row icon">
                                        <div class="col-lg-2 col-md-2 col-sm-2 hidden-xs">
                                            <span class="flaticon flaticon-instagram-logo"></span>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 text-center">
                                            <h6>instagram</h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="row icon">
                                        <div class="col-lg-2 col-md-2 col-sm-2 hidden-xs">
                                            <span class="flaticon flaticon-call"></span>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 text-center">
                                            <h6>call</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="row icon">
                                        <div class="col-lg-2 col-md-2 col-sm-2 hidden-xs">
                                            <span class="flaticon flaticon-telegram"></span>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 text-center">
                                            <h6>telegram</h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="row icon">
                                        <div class="col-lg-2 col-md-2 col-sm-2 hidden-xs">
                                            <span class="flaticon flaticon-close-envelope"></span>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 text-center">
                                            <h6>envelope</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="row icon">
                                        <div class="col-lg-2 col-md-2 col-sm-2 hidden-xs">
                                            <span class="flaticon flaticon-twitter-logo"></span>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 text-center">
                                            <h6>twitter</h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="row icon">
                                        <div class="col-lg-2 col-md-2 col-sm-2 hidden-xs">
                                            <span class="flaticon flaticon-facebook-letter-logo"></span>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 text-center">
                                            <h6>facebook</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row icon-n">
                                <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">
                                    <span class="flaticon flaticon-placeholder"></span>
                                </div>
                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-12 text-center">
                                    <h6>placeholder</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container product">
                <div class="title">
                    <h5>محصولات کفش شهیر</h5>
                </div>
                <div class="pro">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="pr text-center">
                                <img src="images/product1.png" alt="Avatar" class="image">
                                <hr>
                                <h6>کفش تستی شماره ۱</h6>
                                <p>۲۳۰۰۰۰ تومان</p>
                                <div class="overlay">
                                    <div class="text">
                                        <button type="button" class="btn btn-info grey-1">
                                            <span class="flaticon flaticon-supermarket"></span>
                                            افزودن به سبدخرید
                                        </button>
                                        <button type="button" class="btn btn-info grey-3">
                                            <span class="flaticon flaticon-avatar"></span>
                                            مشاهده جزییات
                                        </button>
                                        <div class="grey-2">
                                            سایز ۴۰ تا ۴۵
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="pr text-center">
                                <img src="images/product1.png" alt="Avatar" class="image">
                                <hr>
                                <h6>کفش تستی شماره ۱</h6>
                                <p>۲۳۰۰۰۰ تومان</p>
                                <div class="overlay">
                                    <div class="text">
                                        <button type="button" class="btn btn-info grey-1">
                                            <span class="flaticon flaticon-supermarket"></span>
                                            افزودن به سبدخرید
                                        </button>
                                        <button type="button" class="btn btn-info grey-3">
                                            <span class="flaticon flaticon-avatar"></span>
                                            مشاهده جزییات
                                        </button>
                                        <div class="grey-2">
                                            سایز ۴۰ تا ۴۵
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="pr text-center">
                                <img src="images/product1.png" alt="Avatar" class="image">
                                <hr>
                                <h6>کفش تستی شماره ۱</h6>
                                <p>۲۳۰۰۰۰ تومان</p>
                                <div class="overlay">
                                    <div class="text">
                                        <button type="button" class="btn btn-info grey-1">
                                            <span class="flaticon flaticon-supermarket"></span>
                                            افزودن به سبدخرید
                                        </button>
                                        <button type="button" class="btn btn-info grey-3">
                                            <span class="flaticon flaticon-avatar"></span>
                                            مشاهده جزییات
                                        </button>
                                        <div class="grey-2">
                                            سایز ۴۰ تا ۴۵
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="pr text-center">
                                <img src="images/product1.png" alt="Avatar" class="image">
                                <hr>
                                <h6>کفش تستی شماره ۱</h6>
                                <p>۲۳۰۰۰۰ تومان</p>
                                <div class="overlay">
                                    <div class="text">
                                        <button type="button" class="btn btn-info grey-1">
                                            <span class="flaticon flaticon-supermarket"></span>
                                            افزودن به سبدخرید
                                        </button>
                                        <button type="button" class="btn btn-info grey-3">
                                            <span class="flaticon flaticon-avatar"></span>
                                            مشاهده جزییات
                                        </button>
                                        <div class="grey-2">
                                            سایز ۴۰ تا ۴۵
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="pr text-center">
                                <img src="images/product1.png" alt="Avatar" class="image">
                                <hr>
                                <h6>کفش تستی شماره ۱</h6>
                                <p>۲۳۰۰۰۰ تومان</p>
                                <div class="overlay">
                                    <div class="text">
                                        <button type="button" class="btn btn-info grey-1">
                                            <span class="flaticon flaticon-supermarket"></span>
                                            افزودن به سبدخرید
                                        </button>
                                        <button type="button" class="btn btn-info grey-3">
                                            <span class="flaticon flaticon-avatar"></span>
                                            مشاهده جزییات
                                        </button>
                                        <div class="grey-2">
                                            سایز ۴۰ تا ۴۵
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="pr text-center">
                                <img src="images/product1.png" alt="Avatar" class="image">
                                <hr>
                                <h6>کفش تستی شماره ۱</h6>
                                <p>۲۳۰۰۰۰ تومان</p>
                                <div class="overlay">
                                    <div class="text">
                                        <button type="button" class="btn btn-info grey-1">
                                            <span class="flaticon flaticon-supermarket"></span>
                                            افزودن به سبدخرید
                                        </button>
                                        <button type="button" class="btn btn-info grey-3">
                                            <span class="flaticon flaticon-avatar"></span>
                                            مشاهده جزییات
                                        </button>
                                        <div class="grey-2">
                                            سایز ۴۰ تا ۴۵
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="pr text-center">
                                <img src="images/product1.png" alt="Avatar" class="image">
                                <hr>
                                <h6>کفش تستی شماره ۱</h6>
                                <p>۲۳۰۰۰۰ تومان</p>
                                <div class="overlay">
                                    <div class="text">
                                        <button type="button" class="btn btn-info grey-1">
                                            <span class="flaticon flaticon-supermarket"></span>
                                            افزودن به سبدخرید
                                        </button>
                                        <button type="button" class="btn btn-info grey-3">
                                            <span class="flaticon flaticon-avatar"></span>
                                            مشاهده جزییات
                                        </button>
                                        <div class="grey-2">
                                            سایز ۴۰ تا ۴۵
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="pr text-center">
                                <img src="images/product1.png" alt="Avatar" class="image">
                                <hr>
                                <h6>کفش تستی شماره ۱</h6>
                                <p>۲۳۰۰۰۰ تومان</p>
                                <div class="overlay">
                                    <div class="text">
                                        <button type="button" class="btn btn-info grey-1">
                                            <span class="flaticon flaticon-supermarket"></span>
                                            افزودن به سبدخرید
                                        </button>
                                        <button type="button" class="btn btn-info grey-3">
                                            <span class="flaticon flaticon-avatar"></span>
                                            مشاهده جزییات
                                        </button>
                                        <div class="grey-2">
                                            سایز ۴۰ تا ۴۵
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include("blocks/footer.php");?>
        <?php include("blocks/script.php");?>
    </body>
</html>