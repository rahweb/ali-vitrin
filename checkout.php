<html lang="en">
    <?php include("blocks/head.php");?>
    <body>
        <?php include("blocks/menu.php");?>
        <div class="factors">
            <div class="container">
                <br>
                <div class="row">
                    <div class="brad">
                        <ul>
                            <li class="home"><a href="">علی ویترین</a></li>
                            <li class="last"><a>فاکتور</a></li>
                        </ul>
                    </div>
                </div>
                <br> 
                <div class="factorsbox">
                    <div class="topbox">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="width">شماره</th> 
                                    <th>تصویر محصول</th>
                                    <th>عنوان کالا</th> 
                                    <th>تعداد محصول</th> 
                                    <th>تعداد باکس</th> 
                                    <th>قیمت باکس</th> 
                                    <th>قیمت نهایی</th> 
                                    <th class="color">رنگ</th> 
                                    <th>سایز ها</th> 
                                    <th>تامیین کننده</th> 
                                    <th class="work">عملیات</th>
                                </tr>
                            </thead> 
                            <tbody>
                                <tr>
                                    <td class="width">1</td>
                                    <td>
                                        <img src="images/product1.png" alt="">
                                    </td>
                                    <td>تستی</td>
                                    <td>
                                        <input type="number" value="1" class="form-control input-sm text-center" style="width:80%;margin:auto;">
                                    </td>
                                    <td>
                                        <input type="number" value="1" class="form-control input-sm text-center" style="width:80%;margin:auto;">
                                    </td>
                                    <td>۲۰۰۰۰۰۰ریال</td>
                                    <td>۳۰۰۰۰۰۰ریال</td>
                                    <td class="color">قرمز</td>
                                    <td>۴۰,۴۲</td>
                                    <td>تستی</td>
                                    <td class="work">
                                        <ul>
                                            <li>
                                                <button type="button" class="btn btn-info grey-2">تامیین کننده</button>
                                            </li>
                                            <li>
                                                <button type="button" class="btn btn-info grey-1">حذف</button>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div> 
                    <br>
                    <div class="row calculate">
                        <div class="col-md-3 bg-warning">
                            <h4>تعداد کل محصولات: ۱</h4>
                        </div> 
                        <div class="col-md-3 bg-danger">
                            <h4>هزینه کالا: ۱۵۲,۰۰۰</h4>
                        </div> 
                        <div class="col-md-3 bg-warning">
                            <h4>هزینه حمل و نقل: ۰</h4>
                        </div> 
                        <div class="col-md-3 bg-danger">
                            <h4>هزینه مالیات: ۱۰,۰۰۰</h4>
                        </div> 
                        <div class="col-md-12  bg-success text-center">
                            <h3>مبلغ بدون تخفیف:۱۶۵,۰۰۰</h3>
                        </div> 
                        <div class="col-md-12 bg-info text-center">
                            <h3>هزینه حمل: 16,720</h3>
                        </div> 
                        <div class="col-md-12 bg-success text-center">
                            <h3>مبلغ پرداختی: 148,960</h3>
                        </div>
                    </div>
                    <br>
                    <div class="brad">
                        <ul>
                            <li class="home"><a href="">مشخصات خریدار</a></li>
                        </ul>
                    </div>
                    <hr class="hb">
                    <div class="form">
                        <br>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="row">
                                    <input id="name" name="name" type="text" placeholder="نام" class="form-control">
                                </div>
                                <br>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="row">
                                    <input id="name" name="name" type="text" placeholder="شماره ثابت" class="form-control">
                                </div>
                                <br>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="row">
                                    <input id="name" name="name" type="text" placeholder="شماره همراه" class="form-control">
                                </div>
                                <br>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="row">
                                    <input id="name" name="name" type="text" placeholder="کدپستی" class="form-control">
                                </div>
                                <br>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="row">
                                    <input id="name" name="name" type="text" placeholder="خیابان" class="form-control">
                                </div>
                                <br>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="row">
                                    <input id="name" name="name" type="text" placeholder="آدرس" class="form-control">
                                </div>
                                <br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <br>
                                        بانک
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-center">
                                        <select class="form-control form-control-lg" name="category" id="validationCustom03" onchange="ChangecatList()" required="">
                                            <option value="">بانک</option>
                                            <option value="Classroom Instruction and Assessment">بانک</option>
                                            <option value="Curriculum Development and Alignment">بانک</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <br>
                                        شهر
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-center">
                                        <select class="form-control form-control-lg" name="category" id="validationCustom03" onchange="ChangecatList()" required="">
                                            <option value="">شهر</option>
                                            <option value="Classroom Instruction and Assessment">شهر</option>
                                            <option value="Curriculum Development and Alignment">شهر</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <br>
                                        استان
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-center">
                                        <select class="form-control form-control-lg" name="category" id="validationCustom03" onchange="ChangecatList()" required="">
                                            <option value="">استان</option>
                                            <option value="Classroom Instruction and Assessment">استان</option>
                                            <option value="Curriculum Development and Alignment">استان</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                    </div>
                    <br>
                    <div class="bottombox">
                        <table class="table text-center">
                            <tbody>
                                <tr>
                                    <td colspan="3" rowspan="3">
                                        <div class="row">
                                            <div class="col-md-4 col-xs-4">
                                                <h5>مبلغ پرداختی:</h5>
                                            </div> 
                                            <div class="col-md-4 col-xs-4">
                                                <h5>0ریال</h5>
                                            </div>
                                            <div class="col-md-4 col-xs-4">
                                                <form method="POST" action="http://alivitrine.ir/invoice">
                                                    <input type="hidden" name="_token" value="oZN956ZgdwLzlKbnZXBcR8WYj48FgUbMYf9KJsO1"> 
                                                    <button type="submit" class="btn btn-default">ثبت فاکتور</button>
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <?php include("blocks/footer.php");?>
        <?php include("blocks/script.php");?>
    </body>
</html>