<html lang="en">
    <?php include("blocks/head.php");?>
    <body>
        <?php include("blocks/menu.php");?>
        <div class="control-panel">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <?php include("panel/control-panel.php");?>
                    </div>
                    <div class="col-lg-9">
                        <div class="well-l">
                            <div class="">
                                <h4 style="margin-right:3%">تستی</h4>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="main-timeline2">
                                            <div class="timeline">
                                                <span class="icon fa fa-globe"></span>
                                                <a href="#" class="timeline-content">
                                                    <h3 class="title">تستی</h3>
                                                    <p class="description">
                                                        متن تستی متن تستی متن تستی متن تستی متن تستی
                                                    </p>
                                                </a>
                                            </div>
                                            <div class="timeline">
                                                <span class="icon fa fa-rocket"></span>
                                                <a href="#" class="timeline-content">
                                                    <h3 class="title">تستی</h3>
                                                    <p class="description">
                                                        متن تستی متن تستی متن تستی متن تستی متن تستی
                                                    </p>
                                                </a>
                                            </div>
                                            <div class="timeline">
                                                <span class="icon fa fa-briefcase"></span>
                                                <a href="#" class="timeline-content">
                                                    <h3 class="title">تستی</h3>
                                                    <p class="description">
                                                    متن تستی متن تستی متن تستی متن تستی متن تستی
                                                    </p>
                                                </a>
                                            </div>
                                            <div class="timeline">
                                                <span class="icon fa fa-mobile"></span>
                                                <a href="#" class="timeline-content">
                                                    <h3 class="title">تستی</h3>
                                                    <p class="description">
                                                    متن تستی متن تستی متن تستی متن تستی متن تستی
                                                    </p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include("blocks/footer.php");?>
        <?php include("blocks/script.php");?>
    </body>
</html>