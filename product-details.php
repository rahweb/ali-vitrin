<html lang="en">
    <?php include("blocks/head.php");?>
    <body>
        <?php include("blocks/menu.php");?>
        <div class="product-details">
            <div class="container">
                <br>
                <div class="row">
                    <div class="brad">
                        <ul>
                            <li class="home"><a href="">علی ویترین</a></li>
                            <li><a href="">کفش کالج مردانه</a></li>
                            <li><a href="">کفش سیاره</a></li>
                            <li class="last"><a>کفش کالج ستاره دار</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 right" style="float: left !important;">
                        <div class=" pr-h">
                            <div class="show" href="images/product1.png">
                                <img src="images/product1.png" id="show-img" style="height:auto !important">
                            </div>
                            <div class="small-img">
                                <img src="images/online_icon_right@2x.png" class="icon-left" alt="" id="prev-img">
                                <div class="small-container">
                                    <div id="small-img-roll">
                                        <img src="images/product2.png" class="show-small-img" alt="">
                                        <img src="images/product1.png" class="show-small-img" alt="">
                                        <img src="images/product1.png" class="show-small-img" alt="">
                                        <img src="images/product1.png" class="show-small-img" alt="">
                                        <img src="images/product1.png" class="show-small-img" alt="">
                                        <img src="images/product1.png" class="show-small-img" alt="">
                                        <img src="images/product1.png" class="show-small-img" alt="">
                                        <img src="images/product1.png" class="show-small-img" alt="">
                                        
                                    </div>
                                </div>
                                <img src="images/online_icon_right@2x.png" class="icon-right" alt="" id="next-img">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 left" style="float: right !important;">
                        <div class="well">
                            <div class="">
                                <div class="row">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td class="oranga">قیمت عمده</td>
                                                <td class="dark-blue">۲۳۰۰۰۰۰۰</td>
                                            </tr>
                                            <tr>
                                                <td class="oranga">جنس رویه</td>
                                                <td class="dark-blue">چرم طبیعی- <a href="">توضیح کوتاه</a> </td>
                                            </tr>
                                            <tr>
                                                <td class="oranga">آستر</td>
                                                <td class="dark-blue">چرم طبیعی</td>
                                            </tr>
                                            <tr>
                                                <td class="oranga">زیره</td>
                                                <td class="dark-blue">چرم طبیعی</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-bordered bottom">
                                        <thead>
                                            <tr>
                                                <th class="title">قالب بزرگ پا</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>سایز</td>
                                                <td>۴۰</td>
                                                <td>۴۱</td>
                                                <td>۴۲</td>
                                                <td>۴۳</td>
                                                <td>۴۴</td>
                                            </tr>
                                            <tr>
                                                <td>تعداد</td>
                                                <td>۲</td>
                                                <td>۲</td>
                                                <td>۲</td>
                                                <td>۲</td>
                                                <td>۲</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row button">
                                    <div class="col-lg-8">
                                    <button type="button" class="btn btn-info green">افزودن به سبدخرید</button>
                                    </div>
                                    <div class="col-lg-4">
                                    <button type="button" class="btn btn-info dark-blue">اشتراک</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <br>
            <div class="container title">
                <h5>توضیحات محصول</h5>
            </div>
            <div class="row title-e">
                <div class="container">
                    <br>
                    <h4>تستی</h4>
                    <p>
                    شرکت تولیدی کفش شهپر پس از گذشت حدود نیم قرن از فعالیت متمرکز و متخصص در زمینه تولید انواع پایپوش با برخورداری از نیروهای متخصص و کار آمد با استفاده از نوین ترین فرایند های تولیدی در خدمت صنعت کفش کشور می باشد. خط مشی اصلی شرکت تولید محصول با کیفیت بالا در کنار توجه به نیاز مشتریان و شرایط بازار می باشد. 
                    </p>
                </div>
            </div>
            <div class="container title">
                <br>
                <div class="brad">
                    <ul>
                        <li class="home">محصولات مشابه</li>
                        <li><a href="">کفش کالج مردانه</a></li>
                        <li class="last"><a>کفش سیاره</a></li>
                    </ul>
                </div>
            </div>
            <div class="row title-e">
                <div class="container">
                    <?php include("blocks/product.php");?>
                </div>
            </div>
        </div>
        <?php include("blocks/footer.php");?>
        <?php include("blocks/script.php");?>
    </body>
</html>