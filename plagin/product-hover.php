<div class="brand-page">
    <div class="product">
        <div class="pr text-center">
            <img src="images/product1.png" alt="Avatar" class="image">
            <hr>
            <h6>کفش تستی شماره ۱</h6>
            <p>۲۳۰۰۰۰ تومان</p>
            <div class="overlay">
                <div class="text">
                    <button type="button" class="btn btn-info grey-1">
                        افزودن به سبدخرید
                    </button>
                    <button type="button" class="btn btn-info grey-3">
                        مشاهده جزییات
                    </button>
                    <div class="grey-2">
                        سایز ۴۰ تا ۴۵
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>