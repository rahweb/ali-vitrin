<div class="parallax">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 right">
                <div class="top text-center">
                    <h5>درموتورها جستجو بهتر دیده میشوید</h5>
                </div>
                <br>
                <div class="bottom text-center">
                    <h5>تبدیل قیمتها به ارزهای رایج مبادلاتی</h5>
                    <p>نمایش قیمت محصولات براساس تغییرات لحظه ای بازار ارز برای تجار بین الملل</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 center">
                <img src="images/final-tem-010101.png" alt="">
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 left">
                <div class="top text-center">
                    <h5>ترجمه اتوماتیک جزییات محصولات</h5>
                    <p>بدون داشتن مترجم جزییات محصول و توضیحات برندخودرا به سه زبان بین المللی دیگر نمایش دهید</p>
                </div>
                <br>
                <div class="bottom text-center">
                    <h5>فروشگاه آنلاینی که همیشه باز است</h5>
                </div>
            </div>
        </div>
    </div>
</div>