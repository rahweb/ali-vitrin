<div class="topmenu ">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="dropdown to">
                            <div class="dropdown">
                                <button onclick="myFunction()" class="dropbtn">Dropdown</button>
                                <div id="myDropdown" class="dropdown-content">
                                    <a href="#home">Home</a>
                                    <a href="#about">About</a>
                                    <a href="#contact">Contact</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="dropdown">
                            <button onclick="myFunction()" class="dropbtn">Dropdown</button>
                            <div id="myDropdown" class="dropdown-content">
                                <a href="#home">Home</a>
                                <a href="#about">About</a>
                                <a href="#contact">Contact</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-4 hidden-xs"></div>
            <div class="col-lg-4 col-md-4 hidden-sm hidden-xs">
                <?php include("panel/top-panel.php");?>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 time time-date">
                <ul>
                    <li>یکشنبه - ۲ دی ۱۳۹۷</li>
                    <!-- <li>Sunday - 23 December 2018</li> -->
                    <li class="ltr">12 : 34</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<hr class="hr">
<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="img">
                    <a href="#">
                        <img src="images/logo_farsi.png" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
                <div class="image">
                    <img src="images/alivit.jpg" alt="">
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 button">
                <ul class="header-btns">
                    <li>
                        <div class="dropdown">
                            <button class="dropbtn">
                                سبدخرید (۰)  
                                <span class="flaticon flaticon-supermarket"></span>
                            </button>
                            <div class="dropdown-content">
                                <div class="row you">
                                    <div class="col-md-3 img">
                                        <img src="images/product1.png" alt="">
                                    </div>
                                    <div class="col-md-9 text">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <div class="title">
                                                    <p>نام محصول</p>
                                                    <p>123,000</p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 padding">
                                                <div class="close">
                                                    <a href=""><img src="images/error.png" alt=""></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row factor">
                                    <br>
                                    <div class="col-md-6">
                                        <h6>جمع:</h6>
                                        <hr>
                                        <h6>مالیات:</h6>
                                        <hr>
                                        <!-- <h6>حمل ونقل:</h6>
                                        <hr> -->
                                        <h6>مبلغ پرداختی:</h6>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>۱,۲۳۹,۰۰۰ ریال</h6>
                                        <hr>
                                        <h6>۱۱۵,۰۰۰ ریال</h6>
                                        <hr>
                                        <!-- <h6>۰ ریال</h6>
                                        <hr> -->
                                        <h6>۱,۳۵۴,۰۰۰ ریال</h6>
                                    </div>
                                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModalLong">نمایش سبد خرید و پرداخت</button>
                                    <br>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <button type="button" class="btn btn-info grey-1">
                            <span class="flaticon flaticon-avatar"></span>
                        </button>
                    </li>
                    <li>
                        <button type="button" class="btn btn-info grey-2">
                            <span class="flaticon flaticon-robot"></span>
                        </button>
                    </li>
                </ul>
            </div>
            <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">محصولات</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="img">
                                        <img src="images/product2.png" alt="">
                                        <h5>نام محصول</h5>
                                        <p>۱۲۰۰۰۰ریال</p>
                                        <button type="button" class="btn btn-primary grey-1 he">انتخاب</button>
                                        <br>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="img">
                                        <img src="images/product2.png" alt="">
                                        <h5>نام محصول</h5>
                                        <p>۱۲۰۰۰۰ریال</p>
                                        <button type="button" class="btn btn-primary grey-1 he">انتخاب</button>
                                        <br>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="img">
                                        <img src="images/product2.png" alt="">
                                        <h5>نام محصول</h5>
                                        <p>۱۲۰۰۰۰ریال</p>
                                        <button type="button" class="btn btn-primary grey-1 he">انتخاب</button>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary grey-1">رفتن به سبدخرید</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="menu">
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top top-nav">
            <div class="container">
                <div class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
                        <span class="flaticon flaticon-list"></span>
                        دسته بندی
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <div class="tab">
                            <button class="tablinks" onmouseover="openCity(event, 'tabmenu-1')">تستی</button>
                            <button class="tablinks" onmouseover="openCity(event, 'tabmenu-2')">تستی</button>
                            <button class="tablinks" onmouseover="openCity(event, 'tabmenu-3')">تستی</button>
                            <button class="tablinks" onmouseover="openCity(event, 'tabmenu-4')">تستی</button>
                            <button class="tablinks" onmouseover="openCity(event, 'tabmenu-5')">تستی</button>
                        </div>
                        <div id="tabmenu-1" class="tabcontent">
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="row">
                                        <div class="col-lg-6" style="margin-bottom: 0rem;">
                                            <a href=""><span class="flaticon flaticon-avatar"></span>  تستی</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <img src="images/pro1.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div id="tabmenu-2" class="tabcontent">
                            <h3>Paris</h3>
                            <p>Paris is the capital of France.</p> 
                        </div>
                        <div id="tabmenu-3" class="tabcontent">
                            <h3>Tokyo</h3>
                            <p>Tokyo is the capital of Japan.</p>
                        </div>
                        <div id="tabmenu-4" class="tabcontent">
                            <h3>Tokyo</h3>
                            <p>Tokyo is the capital of Japan.</p>
                        </div>
                        <div id="tabmenu-5" class="tabcontent">
                            <h3>Tokyo</h3>
                            <p>Tokyo is the capital of Japan.</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">

                        <li class="nav-item">
                            <a class="nav-link" href="#">برندها</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">فروشگاه</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">راهنمای خرید</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">راهنمای فروشنده</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">قوانین و مقررات</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">درباره ما</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">تماس باما</a>
                        </li>
                    </ul>
                    <a href="#" class="" style="width:2.35%">
                        <img src="images/icon/home.png" alt="" style="width:100%">
                    </a>
                    <button class="openBtn" onclick="openSearch()">
                        <span class="flaticon flaticon-search"></span>
                    </button>
                </div>
            </div>
        </nav>
        <div id="myOverlay" class="overlay">
            <span class="closebtn" onclick="closeSearch()" title="Close Overlay">×</span>
            <div class="overlay-content">
                <form action="/action_page.php">
                    <input type="text" placeholder="جستجو . . ." name="search">
                    <button type="submit">
                        <span class="flaticon flaticon-search"></span>
                    </button>
                </form>
            </div>
        </div>
</div>




