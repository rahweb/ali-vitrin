<div class="social">
    <div class="container">
        <div class="row text-center">
            <ul>
                <li><a href="#"><span class="flaticon flaticon-twitter"></span></a></li>
                <li><a href="#"><span class="flaticon flaticon-instagram"></span></a></li>
                <li><a href="#"><span class="flaticon flaticon-telegram"></span></a></li>
                <li><a href="#"><span class="flaticon flaticon-google-plus"></span></a></li>
                <li><a href="#"><span class="flaticon flaticon-facebook"></span></a></li>
            </ul>
        </div>
    </div>
</div>