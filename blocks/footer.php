<div class="footer">
    <div class="container">
        <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 right">
            <br>
            <br>
            <br>
            <div class="row ul-l">
                <ul>
                    <li><p>شماره تماس : <a>۰۲۱۷۷۶۶۵۵۴۴</a></p></li>
                    <br>
                    <li><p>شماره تماس : <a>۰۲۱۷۷۶۶۵۵۴۴</a></p></li>
                    <br>
                    <li><p>ایمیل فروش : <a href="#">SALES@ALIVITRINE.IR</a></p></li>
                    <br>
                    <li><p>ایمیل پشتیبانی : <a href="#">SALES@ALIVITRINE.IR</a></p></li>
                    <br>
                    <li><p>بات گرام : <a href="#">SALES@ALIVITRINE.IR</a></p></li>
                </ul>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="row Image text-center">
                <img src="images/logo_farsi-footer.png" alt="">
            </div>
            <br>
            <div class="row namad text-center">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="sabt">
                        <img src="images/Layer 52.png" alt="">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="melli">
                        <img src="images/Layer 53.png" alt="">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="tosee">
                        <img src="images/Layer 51.png" alt="">
                    </div>
                </div>
            </div>
            <br>
            <div class="row eftekhar text-center">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <img src="images/logo_bazargani.png" alt="">
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <img src="images/ef1.png" alt="">
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <img src="images/ef2.png" alt="">
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <img src="images/logo_bazargani2.png" alt="">
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 left">
            <br>
            <br>
            <br>
            <div class="row ul-l">
                <ul>
                    <li><p><a href="#">راهنمای خریداران عمده کفش</a></p></li>
                    <br>
                    <li><p><a href="#">تبلیغات و فروش در علی ویترین</a></p></li>
                    <br>
                    <li><p><a href="#">عضویت رایگان در بات گرام</a></p></li>
                </ul>
            </div>
        </div>
        </div>
        <br>
    </div>
    <div class="row bottomfooter text-center">
        <p>استفاده از مطالب فروشگاه اینترنتی دیجی کالا فقط برای مقاصد غیرتجاری وباذکرمنبع بلامانع است.کلیه حقوق این سایت متعلق به شرکت نوآوران فن آوازه (فروشگاه آنلاین دیجیکالا) میباشد.</p>
    </div>
</div>