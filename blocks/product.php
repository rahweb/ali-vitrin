<div class="product ">
    <div class="blog">
        <div id="blogCarousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#blogCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#blogCarousel" data-slide-to="1"></li>
                <li data-target="#blogCarousel" data-slide-to="2"></li>
            </ol>
            <!-- Carousel items -->
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="row">
                        <div class="col-md-3">
                            <?php include("plagin/product-hover.php");?>
                        </div>
                        <div class="col-md-3">
                            <?php include("plagin/product-hover.php");?>
                        </div>
                        <div class="col-md-3">
                            <?php include("plagin/product-hover.php");?>
                        </div>
                        <div class="col-md-3">
                            <?php include("plagin/product-hover.php");?>
                        </div>
                    </div>
                    <!--.row-->
                </div>
                <!--.item-->
                <div class="carousel-item">
                    <div class="row">
                        <div class="col-md-3">
                            <?php include("plagin/product-hover.php");?>
                        </div>
                        <div class="col-md-3">
                            <?php include("plagin/product-hover.php");?>
                        </div>
                        <div class="col-md-3">
                            <?php include("plagin/product-hover.php");?>
                        </div>
                        <div class="col-md-3">
                            <?php include("plagin/product-hover.php");?>
                        </div>
                    </div>
                    <!--.row-->
                </div>
                <!--.item-->
                <div class="carousel-item">
                    <div class="row">
                        <div class="col-md-3">
                            <?php include("plagin/product-hover.php");?>
                        </div>
                        <div class="col-md-3">
                            <?php include("plagin/product-hover.php");?>
                        </div>
                        <div class="col-md-3">
                            <?php include("plagin/product-hover.php");?>
                        </div>
                        <div class="col-md-3">
                            <?php include("plagin/product-hover.php");?>
                        </div>
                    </div>
                    <!--.row-->
                </div>
                <!--.item-->
            </div>
            <!--.carousel-inner-->
        </div>
        <!--.Carousel-->
    </div>
</div>