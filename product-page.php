<html lang="en">
    <?php include("blocks/head.php");?>
    <body>
        <?php include("blocks/menu.php");?>
        <div class="product-page">
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle"  data-toggle="collapse" data-parent="toggle" href="#collapseOne">
                                    <i class="fa fa-minus"></i> محصولات کفش
                                </a>
                            </div>
                            <div id="collapseOne" class="accordion-body collapse in">
                                <div class="accordion-inner">
                                    <div class="row">
                                        <div class="tab">
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-1')">پوتین و ایمنی</button>
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-2')">طبی زنانه</button>
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-3')">مجلسی مردانه</button>
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-4')">مجلسی مردانه</button>
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-5')">مجلسی مردانه</button>
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-6')">مجلسی مردانه</button>
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-7')">مجلسی مردانه</button>
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-8')">مجلسی مردانه</button>
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-9')">مجلسی مردانه</button>
                                        </div>
                                        <div id="tab-1" class="tabcontent">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <?php include("plagin/product-hover.php");?>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <?php include("plagin/product-hover.php");?>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <?php include("plagin/product-hover.php");?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="tab-2" class="tabcontent">
                                            <h3>Paris</h3>
                                            <p>Paris is the capital of France.</p> 
                                        </div>
                                        <div id="tab-3" class="tabcontent">
                                            <h3>Tokyo</h3>
                                            <p>Tokyo is the capital of Japan.</p>
                                        </div>
                                        <div id="tab-4" class="tabcontent">
                                            <h3>Tokyo</h3>
                                            <p>Tokyo is the capital of Japan.</p>
                                        </div>
                                        <div id="tab-5" class="tabcontent">
                                            <h3>Tokyo</h3>
                                            <p>Tokyo is the capital of Japan.</p>
                                        </div>
                                        <div id="tab-6" class="tabcontent">
                                            <h3>Tokyo</h3>
                                            <p>Tokyo is the capital of Japan.</p>
                                        </div>
                                        <div id="tab-7" class="tabcontent">
                                            <h3>Tokyo</h3>
                                            <p>Tokyo is the capital of Japan.</p>
                                        </div>
                                        <div id="tab-8" class="tabcontent">
                                            <h3>Tokyo</h3>
                                            <p>Tokyo is the capital of Japan.</p>
                                        </div>
                                        <div id="tab-9" class="tabcontent">
                                            <h3>Tokyo</h3>
                                            <p>Tokyo is the capital of Japan.</p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="toggle" href="#collapseTwo">
                                    <i class="fa fa-minus"></i> مواداولیه
                                </a>
                            </div>
                            <div id="collapseTwo" class="accordion-body collapse in">
                                <div class="accordion-inner">
                                    <div class="row">
                                        <div class="tab">
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-1')">پوتین و ایمنی</button>
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-2')">طبی زنانه</button>
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-3')">مجلسی مردانه</button>
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-4')">مجلسی مردانه</button>
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-5')">مجلسی مردانه</button>
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-6')">مجلسی مردانه</button>
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-7')">مجلسی مردانه</button>
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-8')">مجلسی مردانه</button>
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-9')">مجلسی مردانه</button>
                                        </div>
                                        <div id="tab-1" class="tabcontent">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <?php include("plagin/product-hover.php");?>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <?php include("plagin/product-hover.php");?>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <?php include("plagin/product-hover.php");?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="tab-2" class="tabcontent">
                                            <h3>Paris</h3>
                                            <p>Paris is the capital of France.</p> 
                                        </div>
                                        <div id="tab-3" class="tabcontent">
                                            <h3>Tokyo</h3>
                                            <p>Tokyo is the capital of Japan.</p>
                                        </div>
                                        <div id="tab-4" class="tabcontent">
                                            <h3>Tokyo</h3>
                                            <p>Tokyo is the capital of Japan.</p>
                                        </div>
                                        <div id="tab-5" class="tabcontent">
                                            <h3>Tokyo</h3>
                                            <p>Tokyo is the capital of Japan.</p>
                                        </div>
                                        <div id="tab-6" class="tabcontent">
                                            <h3>Tokyo</h3>
                                            <p>Tokyo is the capital of Japan.</p>
                                        </div>
                                        <div id="tab-7" class="tabcontent">
                                            <h3>Tokyo</h3>
                                            <p>Tokyo is the capital of Japan.</p>
                                        </div>
                                        <div id="tab-8" class="tabcontent">
                                            <h3>Tokyo</h3>
                                            <p>Tokyo is the capital of Japan.</p>
                                        </div>
                                        <div id="tab-9" class="tabcontent">
                                            <h3>Tokyo</h3>
                                            <p>Tokyo is the capital of Japan.</p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="toggle" href="#collapseThree">
                                    <i class="fa fa-minus"></i> خدمات تولید
                                </a>
                            </div>
                            <div id="collapseThree" class="accordion-body collapse in">
                                <div class="accordion-inner">
                                    <div class="row">
                                        <div class="tab">
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-1')">پوتین و ایمنی</button>
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-2')">طبی زنانه</button>
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-3')">مجلسی مردانه</button>
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-4')">مجلسی مردانه</button>
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-5')">مجلسی مردانه</button>
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-6')">مجلسی مردانه</button>
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-7')">مجلسی مردانه</button>
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-8')">مجلسی مردانه</button>
                                            <button class="tablinks" onmouseover="openCity(event, 'tab-9')">مجلسی مردانه</button>
                                        </div>
                                        <div id="tab-1" class="tabcontent">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <?php include("plagin/product-hover.php");?>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <?php include("plagin/product-hover.php");?>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <?php include("plagin/product-hover.php");?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="tab-2" class="tabcontent">
                                            <h3>Paris</h3>
                                            <p>Paris is the capital of France.</p> 
                                        </div>
                                        <div id="tab-3" class="tabcontent">
                                            <h3>Tokyo</h3>
                                            <p>Tokyo is the capital of Japan.</p>
                                        </div>
                                        <div id="tab-4" class="tabcontent">
                                            <h3>Tokyo</h3>
                                            <p>Tokyo is the capital of Japan.</p>
                                        </div>
                                        <div id="tab-5" class="tabcontent">
                                            <h3>Tokyo</h3>
                                            <p>Tokyo is the capital of Japan.</p>
                                        </div>
                                        <div id="tab-6" class="tabcontent">
                                            <h3>Tokyo</h3>
                                            <p>Tokyo is the capital of Japan.</p>
                                        </div>
                                        <div id="tab-7" class="tabcontent">
                                            <h3>Tokyo</h3>
                                            <p>Tokyo is the capital of Japan.</p>
                                        </div>
                                        <div id="tab-8" class="tabcontent">
                                            <h3>Tokyo</h3>
                                            <p>Tokyo is the capital of Japan.</p>
                                        </div>
                                        <div id="tab-9" class="tabcontent">
                                            <h3>Tokyo</h3>
                                            <p>Tokyo is the capital of Japan.</p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
        </div>
        <?php include("blocks/footer.php");?>
        <?php include("blocks/script.php");?>
    </body>
</html>