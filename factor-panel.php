<html lang="en">
    <?php include("blocks/head.php");?>
    <body>
        <?php include("blocks/menu.php");?>
        <div class="factor-panel">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <?php include("panel/control-panel.php");?>
                    </div>
                    <div class="col-lg-9 well-l">
                        <div class="well-l">
                            <div class="row" style="margin-right: 2%;padding-top: 2%;">
                                <h4>تغییر رمز عبور</h4>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="right">
                                        <p>اطلاعات کاربر</p>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <ul>
                                                    <li>نام کاربر :</li>
                                                    <li>کد کاربر :</li>
                                                    <li>شماره همراه :</li>
                                                    <li>وضعیت فاکنور :</li>
                                                    <li>تاریخ فاکنور :</li>
                                                </ul>
                                            </div>
                                            <div class="col-lg-6">
                                                <ul>
                                                    <li>سید</li>
                                                    <li>360</li>
                                                    <li>۰۹۱۲۲۳۲۳۲۳۳</li>
                                                    <li>پیش فاکنور</li>
                                                    <li>۲۱:۲۴یکشنبه۲/۱۰/۱۳۹۷</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="left">
                                        <p>اطلاعات فاکنور</p>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <ul>
                                                    <li>مبلغ فاکنور :</li>
                                                    <li>هزینه حمل :</li>
                                                    <li>وضعیت پرداخت :</li>
                                                </ul>
                                            </div>
                                            <div class="col-lg-6">
                                                <ul>
                                                    <li>۱۲/۰۰۰/۰۰۰</li>
                                                    <li>۰</li>
                                                    <li>پرداخت شد</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="fff">
                                <div class="container">
                                    <div class="row">
                                        <div class="table-responsive table-bordered movie-table">
                                            <table class="table movie-table">
                                                <tbody>
                                                    <!--row-->
                                                    <tr class="dark-row">
                                                        <td>کدمحصول</td>
                                                        <td>۲۰۶۷</td>                                       
                                                    </tr>
                                                    <!--/.row-->
                                                    <!--row-->
                                                    <tr class="light-row">
                                                        <td>عنوان محصول</td>
                                                        <td>تلاش</td>                                       
                                                    </tr>
                                                    <!--/.row-->
                                                    <!--row-->
                                                    <tr class ="dark-row">
                                                        <td>رنگ</td>
                                                        <td>سرمه ای</td>                                       
                                                    </tr>
                                                    <!--/.row-->
                                                    <!--row-->
                                                    <tr class="light-row">
                                                        <td>مبلغ واحد</td>
                                                        <td>۱/۲۰۰/۰۰۰ ریال</td>                                       
                                                    </tr>
                                                    <!--/.row-->
                                                    <!--row-->
                                                    <tr class="dark-row">
                                                        <td>تعداد</td>
                                                        <td> ۱ / ۱۲</td>
                                                    </tr>
                                                    <!--/.row-->
                                                    <!--row-->
                                                    <tr class="light-row">
                                                        <td>مبلغ کل</td>
                                                        <td>۱/۲۰۰/۰۰۰</td>                                       
                                                    </tr>
                                                    <!--/.row-->
                                                    <!--row-->
                                                    <tr class="dark-row">
                                                        <td>تامیین کننده</td>
                                                        <td>کفش مصطفی</td>                                       
                                                    </tr>
                                                    <!--/.row-->
                                                    <!--row-->
                                                    <tr class="light-row">
                                                        <td>وضعیت</td>
                                                        <td>
                                                            <button type="" class="btn btn-info grey-2">پیش فاکتور</button>
                                                        </td>                                       
                                                    </tr>
                                                    <!--/.row-->
                                                    <!--row-->
                                                    <tr class="dark-row">
                                                        <td>تایید موجودی</td>
                                                        <td>
                                                            <button type="" class="btn btn-info grey-1">تایید</button>
                                                        </td>                                       
                                                    </tr>
                                                    <!--/.row-->
                                                    <!--row-->
                                                    <tr class="light-row">
                                                        <td>لغو</td>
                                                        <td>
                                                        <select class="form-control form-control-lg" name="category" id="validationCustom03" onchange="ChangecatList()" required="">
                                                            <option value="">عدم موجودی</option>
                                                            <option value="Classroom Instruction and Assessment">شهر</option>
                                                            <option value="Curriculum Development and Alignment">شهر</option>
                                                        </select>
                                                        </td>                                       
                                                    </tr>
                                                    <!--/.row-->
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <button type="button" class="btn btn-info grey-2">ذخیره</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include("blocks/footer.php");?>
        <?php include("blocks/script.php");?>
    </body>
</html>