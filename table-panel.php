<html lang="en">
    <?php include("blocks/head.php");?>
    <body>
        <?php include("blocks/menu.php");?>
        <div class="table-panel">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <?php include("panel/control-panel.php");?>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 well-l">
                        <div class="row" style="margin-right: 2%;padding-top: 2%;">
                            <h4>همه سفارشات</h4>
                        </div>
                        <!-- <button type="button" class="btn btn-info grey-1" style="float: left;margin-top: -5%;">جستجو</button> -->
                        <hr>
                        <div class="">
                            <div class="row">
                                <div class="table-responsive table-bordered movie-table">
                                    <table class="table movie-table">
                                        <thead>
                                            <tr class="movie-table-head">
                                                <th class="color">شماره فاکتور</th>
                                                <th class="color">کاربر</th>
                                                <th class="color">مبلغ پرداختی</th>   
                                                <th class="color">تخفیف</th>
                                                <th class="color">حمل و نقل</th>
                                                <th class="color">کد پیگیری</th>
                                                <th class="color">وضعیت فاکتور</th>
                                                <th class="color">تاریخ</th>
                                                <th class="color">عملیات</th>
                                            </tr>
                                        </thead>   
                                        <tbody>
                                            <!--row-->
                                            <tr class= "dark-row">
                                                <td>۱۷</td>
                                                <td>-۳۰۴</td>
                                                <td>۱۴/۰۰۰/۰۰۰</td>                                     
                                                <td>۰</td>                                     
                                                <td>۰</td>                                     
                                                <td></td>                                     
                                                <td>
                                                    <span class="label label-info">پیش فاکتور</span>
                                                </td>                                     
                                                <td>۱۳۹۷/۱۰/۰۲
                                                    <br>    
                                                    ۲۱:۲۴</td>                                     
                                                <td>
                                                    <button type="button" class="btn btn-info grey-1">مشاهده</button>
                                                </td>                                     
                                            </tr>
                                            <!--/.row-->
                                            <!--row-->
                                            <tr class= "dark-row">
                                                <td>۱۷</td>
                                                <td>-۳۰۴</td>
                                                <td>۱۴/۰۰۰/۰۰۰</td>                                     
                                                <td>۰</td>                                     
                                                <td>۰</td>                                     
                                                <td></td>                                     
                                                <td>
                                                    <span class="label label-info">پیش فاکتور</span>
                                                </td>                                     
                                                <td>۱۳۹۷/۱۰/۰۲
                                                    <br>    
                                                    ۲۱:۲۴</td>                                     
                                                <td>
                                                    <button type="button" class="btn btn-info grey-1">مشاهده</button>
                                                </td>                                     
                                            </tr>
                                            <!--/.row-->
                                            <!--row-->
                                            <tr class= "dark-row">
                                                <td>۱۷</td>
                                                <td>-۳۰۴</td>
                                                <td>۱۴/۰۰۰/۰۰۰</td>                                     
                                                <td>۰</td>                                     
                                                <td>۰</td>                                     
                                                <td></td>                                     
                                                <td>
                                                    <span class="label label-info">پیش فاکتور</span>
                                                </td>                                     
                                                <td>۱۳۹۷/۱۰/۰۲
                                                    <br>    
                                                    ۲۱:۲۴</td>                                     
                                                <td>
                                                    <button type="button" class="btn btn-info grey-1">مشاهده</button>
                                                </td>                                     
                                            </tr>
                                            <!--/.row-->
                                            <!--row-->
                                            <tr class= "dark-row">
                                                <td>۱۷</td>
                                                <td>-۳۰۴</td>
                                                <td>۱۴/۰۰۰/۰۰۰</td>                                     
                                                <td>۰</td>                                     
                                                <td>۰</td>                                     
                                                <td></td>                                     
                                                <td>
                                                    <span class="label label-info">پیش فاکتور</span>
                                                </td>                                     
                                                <td>۱۳۹۷/۱۰/۰۲
                                                    <br>    
                                                    ۲۱:۲۴</td>                                     
                                                <td>
                                                    <button type="button" class="btn btn-info grey-1">مشاهده</button>
                                                </td>                                     
                                            </tr>
                                            <!--/.row-->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include("blocks/footer.php");?>
        <?php include("blocks/script.php");?>
    </body>
</html>