<html lang="en">
    <?php include("blocks/head.php");?>
    <body>
        <?php include("blocks/menu.php");?>
        <div class="buyer">
            <div class="container">
                <br>
                <div class="row">
                    <div class="brad">
                        <ul>
                            <li class="home"><a href="">علی ویترین</a></li>
                            <li class="last"><a>راهنمای خریداران</a></li>
                        </ul>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="title">
                        <h5>فرایند خرید در سایت</h5>
                        <hr class="hr">
                        <p>در سایت علی ویترین خریداران عمده ی کفش میتوانند براحتی جدیدترین محصولات برندهای مختلف کفش ایرانی را مشاهده و بدون نیاز به مراجعه حضوری به قیمت کارخانه
                            از طریق سایت اقدام به خرید نمایید که شامل مراحل زیر میباشد.
                        </p>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="title">
                        <h5>امکانات سایت دربخش تبلیغات وفروش</h5>
                        <hr class="hr">
                    </div>
                    <div class="row icon text-center">
                        <div class="col-lg col-md col-sm-6 col-xs-12">
                            <img src="images/icon/marketing.png" alt="">
                            <hr>
                            <p>
                               ۱. اعلام نیاز و ثبت سفارش
                            </p>
                        </div>
                        <div class="col-lg col-md col-sm-6 col-xs-12">
                            <img src="images/icon/support.png" alt="">
                            <hr>
                            <p>
                               ۲. کنترل موجودی سفارش توسط تامین کننده و صدور فاکتورظرف یک روز کاری.
                            </p>
                        </div>
                        <div class="col-lg col-md col-sm-6 col-xs-12">
                            <img src="images/icon/wallet.png" alt="">
                            <hr>
                            <p>
                                ۳.صدور فاکتور و پرداخت آنلاین توسط خریدار که مهلت پرداخت خریدار ۲۴ ساعت میباشد.
                            </p>
                        </div>
                        <div class="col-lg col-md col-sm-6 col-xs-12">
                            <img src="images/icon/truck.png" alt="">
                            <hr>
                            <p>
                                ۴.ارسال سفارش به باربری برای حمل.
                            </p>
                        </div>
                        <div class="col-lg col-md col-sm-6 col-xs-12">
                            <img src="images/icon/return(1).png" alt="">
                            <hr>
                            <p>
                               ۵. خریدار پس از تحویل سفارش توسط پست و یا رسیدن سفارش به باربری مقصد تا دو روز کاری، فرصت بررسی آن را از نظر معیوب و یا مغایر سفارش بودن سفارش را دارد که در این صورت امکان مرجوع کردن توسط خریدار وجود دارد.
                                برای دریافت اطلاع بیشتر به قوانین سایت و  فرآیند خرید و فروش را مطالعه فرمایید.
                            </p>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
        <?php include("blocks/footer.php");?>
        <?php include("blocks/script.php");?>
    </body>
</html>