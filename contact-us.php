<html lang="en">
    <?php include("blocks/head.php");?>
    <body>
        <?php include("blocks/menu.php");?>
        <div class="about-us">
            <div class="container">
                <div class="row">
                    <div class="brad">
                        <br>
                        <ul>
                            <li class="home"><a href="">علی ویترین</a></li>
                            <li class="last"><a>تماس باما</a></li>
                        </ul>
                        <br>
                    </div>
                    <div class="row contact-us">
                        <div class="col-md-12 col-sm-12" style="padding:0%">
                            <!-- SECTION TITLE -->
                            <div class="wow fadeInUp section-title" data-wow-delay="0.2s">
                                <h4>تماس باما</h4>
                                <hr class="hr">
                                <br>
                            </div>
                        </div>
                        <div style="width:100%" id="contact" class="map">
                            <iframe style="width:100%;height: 300px;" src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3149.9195705182588!2d51.42086638796755!3d35.70003600813093!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1541621317440" style="border:0" allowfullscreen="" width="1400" height="600" frameborder="0">    </iframe>
                        </div>
                        <div class="col-md-7 col-sm-10">
                                <!-- CONTACT FORM HERE -->
                                <div class="wow fadeInUp" data-wow-delay="0.4s">
                                    <form id="contact-form" action="#" method="get">
                                        <div class="col-md-6 col-sm-6">
                                            <input type="text" class="form-control" name="name" placeholder="نوشتن نام" required="">
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <input type="email" class="form-control" name="email" placeholder="نوشتن ایمیل" required="">
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <textarea class="form-control" rows="5" name="message" placeholder="نوشتن پیام" required=""></textarea>
                                        </div>
                                        <div class="col-md-offset-8 col-md-4 col-sm-offset-6 col-sm-6" style="float: left;">
                                            <button id="submit" type="submit" class="form-control" name="submit"><img style="width:20%" src="images/mail.png" alt="">  ارسال پیام</button>
                                        </div>
                                    </form>
                                </div>
                        </div>
                        <div class="col-md-5 col-sm-8">
                            <!-- CONTACT INFO -->
                            <div class="wow fadeInUp contact-info" data-wow-delay="0.4s">
                                <br>
                                <br>
                                <p>
                                    <svg style="width: 5%;color: #fe6a00;" aria-hidden="true" data-prefix="fas" data-icon="phone" class="svg-inline--fa fa-phone fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                        <path fill="currentColor" d="M493.4 24.6l-104-24c-11.3-2.6-22.9 3.3-27.5 13.9l-48 112c-4.2 9.8-1.4 21.3 6.9 28l60.6 49.6c-36 76.7-98.9 140.5-177.2 177.2l-49.6-60.6c-6.8-8.3-18.2-11.1-28-6.9l-112 48C3.9 366.5-2 378.1.6 389.4l24 104C27.1 504.2 36.7 512 48 512c256.1 0 464-207.5 464-464 0-11.2-7.7-20.9-18.6-23.4z"></path>
                                    </svg>
                                    <span style="color: #fe6a00;">تلفن :</span>
                                    02166324766
                                </p>
                                <p>
                                    <svg style="width: 5%;color: #fe6a00;" aria-hidden="true" data-prefix="fas" data-icon="envelope-open" class="svg-inline--fa fa-envelope-open fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                        <path fill="currentColor" d="M512 464c0 26.51-21.49 48-48 48H48c-26.51 0-48-21.49-48-48V200.724a48 48 0 0 1 18.387-37.776c24.913-19.529 45.501-35.365 164.2-121.511C199.412 29.17 232.797-.347 256 .003c23.198-.354 56.596 29.172 73.413 41.433 118.687 86.137 139.303 101.995 164.2 121.512A48 48 0 0 1 512 200.724V464zm-65.666-196.605c-2.563-3.728-7.7-4.595-11.339-1.907-22.845 16.873-55.462 40.705-105.582 77.079-16.825 12.266-50.21 41.781-73.413 41.43-23.211.344-56.559-29.143-73.413-41.43-50.114-36.37-82.734-60.204-105.582-77.079-3.639-2.688-8.776-1.821-11.339 1.907l-9.072 13.196a7.998 7.998 0 0 0 1.839 10.967c22.887 16.899 55.454 40.69 105.303 76.868 20.274 14.781 56.524 47.813 92.264 47.573 35.724.242 71.961-32.771 92.263-47.573 49.85-36.179 82.418-59.97 105.303-76.868a7.998 7.998 0 0 0 1.839-10.967l-9.071-13.196z"></path>
                                    </svg>
                                    <span style="color: #fe6a00;">ایمیل :</span>
                                    <a href="" style="color:#000;">info@alivitrine.ir</a>
                                </p>
                                <p>
                                    <svg style="width: 5%;color: #fe6a00;" aria-hidden="true" data-prefix="fas" data-icon="map-marker-alt" class="svg-inline--fa fa-map-marker-alt fa-w-12" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
                                        <path fill="currentColor" d="M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z"></path>
                                    </svg>    
                                    <span style="color: #fe6a00;">آدرس :</span>
                                    تهران چهار راه یافت آباد بلوار معلم شهرک صاحب الزمان خ رضایی پ ۹۳
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include("blocks/footer.php");?>
        <?php include("blocks/script.php");?>
    </body>
</html>