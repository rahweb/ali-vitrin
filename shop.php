<html lang="en">
    <?php include("blocks/head.php");?>
    <body>
        <?php include("blocks/menu.php");?>

        <div class="shop shop-s">
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 lg">
                        <div class="title text-center title-filter">
                            <p>کفش پیاده روی و ورزشی</p>
                        </div>
                        <div id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="card">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <div class="card-header" role="tab" id="headingOne">
                                        <h6 class="mb-0">
                                                برندها
                                        </h6>
                                    </div>
                                </a>
                                <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="card-block text-center">
                                        <form action="">
                                            <div class="row">
                                                <div class="col-md-2 checkbox">
                                                    <input type="checkbox" name="gender" value="male">
                                                </div>
                                                <div class="col-md-10">
                                                    <lable class="text-right">چرم طبیعی</lable>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2 checkbox">
                                                    <input type="checkbox" name="gender" value="male">
                                                </div>
                                                <div class="col-md-10">
                                                    <lable class="text-right">چرم طبیعی</lable>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2 checkbox">
                                                    <input type="checkbox" name="gender" value="male">
                                                </div>
                                                <div class="col-md-10">
                                                    <lable class="text-right">چرم طبیعی</lable>
                                                </div>
                                            </div>
                                           
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                    <div class="card-header" role="tab" id="headingThree">
                                        <h6 class="mb-0">
                                                برندها
                                        </h6>
                                    </div>
                                </a>
                                <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="card-block text-center">
                                        <form action="">
                                            <div class="row">
                                                <div class="col-md-2 checkbox">
                                                    <input type="checkbox" name="gender" value="male">
                                                </div>
                                                <div class="col-md-10">
                                                    <lable class="text-right">چرم طبیعی</lable>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2 checkbox">
                                                    <input type="checkbox" name="gender" value="male">
                                                </div>
                                                <div class="col-md-10">
                                                    <lable class="text-right">چرم طبیعی</lable>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2 checkbox">
                                                    <input type="checkbox" name="gender" value="male">
                                                </div>
                                                <div class="col-md-10">
                                                    <lable class="text-right">چرم طبیعی</lable>
                                                </div>
                                            </div>
                                           
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <div class="card-header" role="tab" id="headingTwo">
                                        <h6 class="mb-0">
                                                قیمت
                                        </h6>
                                    </div>
                                </a>
                                <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="card-block text-center">
                                        collapse
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <h6 class="text-center font-weight-bold blue-text mt-3 mb-4 pb-4" style="text-align:right !important;"><strong>محدوده قیمت مورد نظر</strong></h6>
                                <hr>
                                <form class="range-field my-5" style="text-align:center !important;">
                                    <input id="calculatorSlider" class="no-border" type="range" value="0" min="0" max="30" />
                                </form>
                                <!-- Grid row -->
                                <div class="row">
                                    <!-- Grid column -->
                                    <div class="col-md-12 text-center pb-5">
                                        <h6><span class="badge blue lighten-2 mb-4">تا</span></h6>
                                        <h4 class="" style="color:#0d47a1"><strong id="resellerEarnings">۰</strong></h4>
                                        <br>
                                        <h6><span class="badge blue lighten-2 mb-4">تومان</span></h6>
                                    </div>
                                    <!-- Grid column -->
                                    <!-- Grid column -->
                                    <!-- <div class="col-md-6 text-center pb-5">
                                        <h6><span class="badge blue lighten-2 mb-4">تا</span></h6>
                                        <h4 class="" style="color:#0d47a1"><strong id="resellerEarnings">۰</strong></h4>
                                        <br>
                                        <h6><span class="badge blue lighten-2 mb-4">تومان</span></h6>
                                    </div> -->
                                    <!-- Grid column -->
                                </div>
                                <!-- Grid row -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 lg">
                        <div class="tabs">
                            <div class="tab-button-outer">
                                <ul id="tab-button">
                                    <li><a href="#tab01">ارزانترین</a></li>
                                    <li><a href="#tab02">گرانترین</a></li>
                                    <li><a href="#tab03">جدیدترین</a></li>
                                    <li><a href="#tab04">پرفروشترین</a></li>
                                    <!-- <li><a href="#tab05">Tab 5</a></li> -->
                                </ul>
                            </div>
                            <div class="tab-select-outer">
                                <select id="tab-select">
                                    <option value="#tab01">Tab 1</option>
                                    <option value="#tab02">Tab 2</option>
                                    <option value="#tab03">Tab 3</option>
                                    <option value="#tab04">Tab 4</option>
                                    <!-- <option value="#tab05">Tab 5</option> -->
                                </select>
                            </div>
                            <div id="tab01" class="tab-contents brand-page">
                                <div class="product">
                                    <div class="pro">
                                        <div class="row">
                                           <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">   
                                                <?php include("plagin/product-hover.php");?>
                                            </div> 
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">   
                                                <?php include("plagin/product-hover.php");?>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">   
                                                <?php include("plagin/product-hover.php");?>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">   
                                                <?php include("plagin/product-hover.php");?>
                                            </div> 
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">   
                                                <?php include("plagin/product-hover.php");?>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">   
                                                <?php include("plagin/product-hover.php");?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab02" class="tab-contents brand-page">
                                <div class="product">
                                    <div class="pro">
                                        <div class="row">
                                           <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">   
                                                <?php include("plagin/product-hover.php");?>
                                            </div> 
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">   
                                                <?php include("plagin/product-hover.php");?>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">   
                                                <?php include("plagin/product-hover.php");?>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">   
                                                <?php include("plagin/product-hover.php");?>
                                            </div> 
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">   
                                                <?php include("plagin/product-hover.php");?>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">   
                                                <?php include("plagin/product-hover.php");?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab03" class="tab-contents brand-page">
                                <div class="product">
                                    <div class="pro">
                                        <div class="row">
                                           <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">   
                                                <?php include("plagin/product-hover.php");?>
                                            </div> 
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">   
                                                <?php include("plagin/product-hover.php");?>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">   
                                                <?php include("plagin/product-hover.php");?>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">   
                                                <?php include("plagin/product-hover.php");?>
                                            </div> 
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">   
                                                <?php include("plagin/product-hover.php");?>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">   
                                                <?php include("plagin/product-hover.php");?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab04" class="tab-contents brand-page">
                                <div class="product">
                                    <div class="pro">
                                        <div class="row">
                                           <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">   
                                                <?php include("plagin/product-hover.php");?>
                                            </div> 
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">   
                                                <?php include("plagin/product-hover.php");?>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">   
                                                <?php include("plagin/product-hover.php");?>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">   
                                                <?php include("plagin/product-hover.php");?>
                                            </div> 
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">   
                                                <?php include("plagin/product-hover.php");?>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">   
                                                <?php include("plagin/product-hover.php");?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
        </div>

        <?php include("blocks/footer.php");?>
        <?php include("blocks/script.php");?>
    </body>
</html>